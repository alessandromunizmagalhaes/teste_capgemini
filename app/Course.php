<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Course extends Model
{
    //

    public function category()
    {
      return $this->belongsTo(Category::class);
    }

    public function address()
    {
      return $this->belongsTo(Address::class);
    }

    public function consultant()
    {
      return $this->belongsTo(Consultant::class);
    }
}
