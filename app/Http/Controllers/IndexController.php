<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Category;
use App\Course;

class IndexController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $pesquisa  = session()->get('pesquisa');
        if(strlen($pesquisa) == 0)
        {
          $cursos = Course::with(['category','address'])->get();
        }
        else
        {
          $cursos = Course::with(['category','address'])->orWhere('title','like','%' . $pesquisa . '%')->get();
        }

        return view('cursos')->with(array('cursos'=>$cursos, 'pesquisa' => $pesquisa));
    }

    public function detalhe_curso($id)
    {
      $curso = Course::with(['category','address','consultant'])->find($id);

      return view('detalhe_curso')->with(array('curso'=>$curso));
    }

    public function pesquisa(Request $request)
    {
      $pesquisa = $request->input('pesquisa');

      session()->flash('pesquisa', $pesquisa);

      return $this->index();
    }
}
