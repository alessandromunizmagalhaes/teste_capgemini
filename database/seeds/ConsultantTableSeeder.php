<?php

use Illuminate\Database\Seeder;

class ConsultantTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        factory(App\Consultant::class, 50)->create();
    }
}
