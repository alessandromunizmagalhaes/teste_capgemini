<?php

use Faker\Generator as Faker;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(App\User::class, function (Faker $faker) {
    static $password;

    return [
        'name' => $faker->name,
        'email' => $faker->unique()->safeEmail,
        'password' => $password ?: $password = bcrypt('secret'),
        'remember_token' => str_random(10),
    ];
});

$factory->define(App\Consultant::class, function (Faker $faker) {
    return [
        'name' => $faker->name,
        'avatar' => $faker->imageUrl($width = 70, $height = 70),
    ];
});

$factory->define(App\Category::class, function (Faker $faker) {
    return [
        'category' => $faker->domainName,
    ];
});

$factory->define(App\Address::class, function (Faker $faker) {
    return [
        'street' => $faker->name,
        'number' => $faker->numberBetween(1,100),
        'neighborhood' => str_random(10),
        'city' => $faker->city,
    ];
});

$factory->define(App\Course::class, function (Faker $faker) {
  return [
      'title' => $faker->name,
      'description' => "Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.",
      'price' => $faker->numberBetween(1,1000),
      'start' => date('Y-m-d H:i:s'),
      'finish' => date('Y-m-d H:i:s'),
      'consultant_id' => $faker->numberBetween(1,50),
      'address_id' => $faker->numberBetween(1,50),
      'category_id' => $faker->numberBetween(1,50),
  ];
});
