@extends('layouts.app')

@section('content')

  <div class="container-fluid bg-info app-navbar">
    <nav class="navbar bg-info justify-content-between text-white">
      <a class="navbar-brand">Cursos</a>
      <form id="meu_form" class="form-inline" method="POST" action="{{ url('/') }}/pesquisa">
        {{ csrf_field() }}
        <div class="input-group">
          <span class="input-group-addon">
              <i class="mdi mdi-magnify" aria-hidden="true"></i>
          </span>
          <input type="text" name="pesquisa" value="{{$pesquisa}}" class="form-control" placeholder="Pesquisar" aria-label="Pesquisar">
        </div>
      </form>
    </nav>
  </div>

  <div class="container">
    <div class="row">
      @if(isset($cursos) && count($cursos) > 0)
        @foreach($cursos as $curso)
          <div class="col-md-4 mt-5">
            <div class="card">
                <a href="{{ url('/') }}/detalhe-curso/{{$curso->id}}">
                  <div class="card-body">
                    <h6 class="card-subtitle mb-2 text-muted">{{$curso->category->category}}</h6>
                      <h4 class="card-title">{{$curso->title}}</h4>
                      <p class="card-text text-muted">{{$curso->address->city}}</p>
                  </div>
                  <div class="card-footer text-muted">
                    <div class="row">
                      <div class="col-10">
                          <p>De {{date("d/m/Y", strtotime($curso->start))}} a {{date("d/m/Y", strtotime($curso->end))}}</p>
                      </div>
                      <div class="col-2">
                        <i class="mdi mdi-calendar" aria-hidden="true"></i>
                      </div>
                    </div>
                  </div>
                </a>
              </div>
            </div>
        @endforeach
      @else
      <div class="container my-5 text-center">
          <h4>Nenhum curso encontrado</h4>
      </div>
      @endif
    </div>
  </div>

@endsection
