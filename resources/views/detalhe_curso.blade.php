@extends('layouts.app')

@section('content')

<div class="jumbotron jumbotron-fluid bg-info text-white">
  <div class="container">
    <nav class="navbar bg-info justify-content-between">
      <a class="navbar-brand" href="{{ url('/') }}"><span><i class="mdi mdi-arrow-left" aria-hidden="true"></i></span></a>
    </nav>
  </div>
</div>

<div class="container container-detalhe-curso">
  <div class="card">
    <div class="card-body">
      <h4 class="card-title">{{$curso->title}}</h4>
      <p class="card-text text-muted">{{$curso->description}}</p>
      <p class="card-text text-muted">
        <i class="mdi mdi-calendar" aria-hidden="true"></i>&nbsp;&nbsp;&nbsp;&nbsp;
        {{date("d/m/Y", strtotime($curso->start))}}
      </p>
      <p class="card-text text-muted">
        <i class="mdi mdi-clock" aria-hidden="true"></i>&nbsp;&nbsp;&nbsp;&nbsp;
        {{date("d/m/Y", strtotime($curso->end))}}
      </p>
      <p class="card-text text-muted">
        <i class="mdi mdi-map-marker" aria-hidden="true"></i>&nbsp;&nbsp;&nbsp;&nbsp;
        <a href="#">{{$curso->address->street}} {{$curso->address->number}}, {{$curso->address->neighborhood}}, {{$curso->address->city}}</a>
      </p>
      <p class="card-text text-muted">
        <i class="mdi mdi-currency-usd" aria-hidden="true"></i>&nbsp;&nbsp;&nbsp;&nbsp;
        R${{$curso->price}}
      </p>
      <p class="card-text text-muted">
        <i class="mdi mdi-tag" aria-hidden="true"></i>&nbsp;&nbsp;&nbsp;&nbsp;
        {{$curso->category->category}}
      </p>
      <p class="card-text text-muted">
        <img class="rounded-circle" width="40" height="40" src="{{$curso->consultant->avatar}}">&nbsp;
        {{$curso->consultant->name}}
      </p>
      <div class="text-center">
        <button type="button" class="btn btn-warning">INSCRIÇÃO</button>
      </div>
    </div>
  </div>
</div>

@endsection
